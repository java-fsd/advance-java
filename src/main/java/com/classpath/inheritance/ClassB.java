package com.classpath.inheritance;

public class ClassB extends ClassA {
	
	public ClassB(String name, int value) {
		super(name,value);
	}
	
	public void printValuesFromSuperClass() {
		System.out.println("Value from the super class "+ super.name + " value :: "+ super.value);
	}
	
	public static void main(String[] args) {
		
		ClassB obj = new ClassB("Naveen", 34);
		obj.printValuesFromSuperClass();
	}

}
