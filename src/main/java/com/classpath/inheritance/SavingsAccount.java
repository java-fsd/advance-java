package com.classpath.inheritance;

public class SavingsAccount extends BankAccount {
	
	private static final double MIN_ACCOUNT_BALANCE = 10_000d;
	
	public SavingsAccount(String name, double accountBalance) {
		super(name, accountBalance);
	}

	@Override
	public final void deposit(double amount) {
		this.accountBalance = this.accountBalance + amount;
		
	}

	@Override
	public final double withdraw(double amount) {
		if (this.accountBalance - amount > MIN_ACCOUNT_BALANCE) {
			this.accountBalance = this.accountBalance - amount;
			return amount;
		}
		throw new InsuffficientAccountBalanceException("insufficient funds to withdraw");
		
	}

	@Override
	public final double checkAccountBalance() {
		return this.accountBalance;
	}

}
