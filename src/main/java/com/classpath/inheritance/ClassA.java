package com.classpath.inheritance;

public class ClassA {
	
	 int value;
     String name;
     
     public ClassA(String name, int value) {
    	 this.name = name;
    	 this.value = value;
     }

}
