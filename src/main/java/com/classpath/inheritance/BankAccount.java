package com.classpath.inheritance;

public abstract class BankAccount {
	
	 String name;
	 double accountBalance;
	
	public BankAccount(String name, double accountBalance) {
		this.name = name;
		this.accountBalance = accountBalance;
	}
	
	public abstract void deposit(double amount);

	public abstract double withdraw(double amount);

	public abstract double checkAccountBalance();


}
