package com.classpath.inheritance;

import java.util.Scanner;

public class BankAccountClient {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter your name ::");
		String name = scanner.next();
		
		System.out.println("Please enter the initial account opening balance ::");
		double initialAccountBalance = scanner.nextDouble();
		
		System.out.println("Enter your option :");
		System.out.println(" 1 : Savings Account");
		System.out.println(" 2 : Current Account");
		
		
		
		int option = scanner.nextInt();
		BankAccount bankAccount = null;
		switch(option) {
		case 1:
			bankAccount = new SavingsAccount(name, initialAccountBalance);
			break;
		case 2:
			bankAccount = new CurrentAccount(name, initialAccountBalance);
			break;
		}
		
		System.out.println("Enter the next set of options ::");
		System.out.println("1: Deposit");
		System.out.println("2: Withdraw");
		System.out.println("3: Check balance");
		
		option = scanner.nextInt();
		
		switch(option) {
		case 1:
			System.out.println(" Please enter amount to deposit");
			double amountToBeDeposited = scanner.nextDouble();
			bankAccount.deposit(amountToBeDeposited);
			System.out.println(" The amount  "+ amountToBeDeposited + " is deposited to the bank");
			break;
		case 2:
			System.out.println(" Please enter amount to withdraw");
			double amountToBeWithdrawn = scanner.nextDouble();
			try {
				bankAccount.withdraw(amountToBeWithdrawn);
			} catch(InsuffficientAccountBalanceException exception) {
				System.out.println(" Insufficient balance :: ");
				return;
			}
			System.out.println("Amount withdrawn :: "+ amountToBeWithdrawn);
			break;
		case 3:
			double accountBalance = bankAccount.checkAccountBalance();
			System.out.println(" The current account balance is "+ accountBalance);
			break;
		}
	}
}
