package com.classpath.inheritance;

public class ClassC extends ClassB {

	public ClassC(int number, String name) {
			super(name, number);
	}

	public static void main(String[] args) {

		ClassC obj = new ClassC(23, "Testing::");
		obj.printValuesFromSuperClass();
	}

}
