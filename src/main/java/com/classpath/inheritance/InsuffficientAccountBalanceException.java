package com.classpath.inheritance;

public class InsuffficientAccountBalanceException extends RuntimeException {
	
	public InsuffficientAccountBalanceException(String message) {
		super(message);
	}
	
	@Override
	public String getMessage() {
		return super.getMessage();
	}
	
	

}
