package com.classpath.inheritance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SavingsAccountTests {

	@Test
	void testDeposit() {
		BankAccount savingsAccount = new SavingsAccount("Ramesh", 10000);
		savingsAccount.deposit(200);
		Assertions.assertEquals(savingsAccount.checkAccountBalance(), 10200);
	}

	@Test
	void testNegativeWithdraw() {
		BankAccount savingsAccount = new SavingsAccount("Ramesh", 10000);
		try {
			savingsAccount.withdraw(200);
		} catch (Exception exception) {
			assertNotNull(exception);
			assertTrue(exception instanceof InsuffficientAccountBalanceException);
		}
	}
	

	@Test
	void testWithdraw() {
		BankAccount savingsAccount = new SavingsAccount("Ramesh", 10_000);
		try {
			savingsAccount.deposit(4000);
			savingsAccount.withdraw(200);
			assertEquals(savingsAccount.checkAccountBalance(), 13_800);
		} catch (Exception exception) {
			fail("Should not throw an exception");
		}
	}

}
